import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;

public class WebTechnologyDetector {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		// odczytanie z plików
		Scanner urls = new Scanner(new FileReader("urls.txt"));
		Scanner technologies = new Scanner(new FileReader("technologies.txt"));

		// wczytanie do listy technologii ze strumienia
		List<String> technologiesList = new ArrayList<>();
		while (technologies.hasNextLine())
			technologiesList.add(technologies.nextLine().toLowerCase());

		// przetwarzanie adresów urls
		PrintWriter outFile = new PrintWriter("summary.txt");
		Pattern pattern;
		Matcher matcher;
		String url;
		String headContent;
		while (urls.hasNextLine()) {
			url = urls.nextLine();
			outFile.print(url + ' ');
			headContent = Jsoup.connect(url).get().head().toString().toLowerCase();
			for (String technology : technologiesList) {
				pattern = Pattern.compile("[^a-z]" + technology + "[^a-z]");
				// pattern = Pattern.compile(technology);
				matcher = pattern.matcher(headContent);
				if (matcher.find())
					outFile.print(technology + " ");
			}
			outFile.println();
		}
		outFile.close();
		technologies.close();
		urls.close();
	}

}
